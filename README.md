

###Example with:###
B-L475E-IOT01A (STM32L4 Discovery kit IoT node, low-power wireless, BLE, NFC, SubGHz, Wi-Fi)

###Included:###
* the example test_sensor_hts221 works with the HTS221 and reads temperature and humidity with B-L475E-IOT01A
* the example Test_LSM6DSL works with the LSM6DSL and reads Acceleration, angular velocity and temperature with B-L475E-IOT01A
* Stm32Cube file
###Prerequisites (to compile):###
* KEIL uVision 5
* STM32CubeMX
	
###used Git:###
* https://github.com/STMicroelectronics/STMems_Standard_C_drivers
* http://www.st.com/en/mems-and-sensors.html
###Email: jonathan.cagua@gmail.com###